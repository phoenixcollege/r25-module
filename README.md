## R25/25Live library

This application is licensed under the [MIT License](http://opensource.org/licenses/MIT).

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.4+
* [Composer](https://getcomposer.org/)

### Installation

Add to project with composer

`composer.json`

```
...
"respositories": [
  {
    "url": "https://scott_morken@bitbucket.org/phoenixcollege/r25-module.git",
    "type": "git"
  }
]
...
"require": {
  "phoenixcollege/r25": "~1.0"
}
```