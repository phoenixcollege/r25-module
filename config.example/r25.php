<?php
return [
    'client' => [
        //'test' => true,
        'base_uri' => 'https://25live.baseurl.com/run',
        'options' => [
            'cookies' => true,
            /*'debug' => true,*/
        ],
    ],
    'credentials' => [
        'username' => 'user',
        'password' => 'pw',
    ],
    'services' => [
        'login' => 'R25\Services\Login',
        'space_reservations' => 'R25\Services\SpaceReservations',
    ],
];