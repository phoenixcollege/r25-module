<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 10:13 AM
 */

namespace R25\Contracts\Converter;


use R25\Contracts\Xml\Xml;

interface Converter {

    public function create(Xml $xml);

    public function createFromArray($data);

    public function toArray();

    public function getRaw();

}