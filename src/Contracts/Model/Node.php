<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 12:41 PM
 */

namespace R25\Contracts\Model;


interface Node {

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getValue();

    /**
     * @param string $value
     */
    public function setValue($value);

    /**
     * @return array
     */
    public function getAttributes();

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes);

    /** @return \R25\Contracts\Model\Node[]
     */
    public function getChildren();

    /**
     * @param array $children
     */
    public function setChildren($children);

    public function hasChildren();

    public function addAttribute($key, $value);

    public function getAttribute($key);

    public function addChild(Node $child);

    public function toArray($values_only = false);

}