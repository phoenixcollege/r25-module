<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 9:04 AM
 */

namespace R25\Contracts\Service;


use R25\Contracts\Converter\Converter;

interface Login {

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function login($username, $password);

    /**
     * @param Converter $c
     * @return bool
     */
    public function needsLogin(Converter $c);

}