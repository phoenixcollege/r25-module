<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/28/15
 * Time: 2:04 PM
 */

namespace R25\Contracts\Xml;


interface Xml {

    public function createXmlObject($xml_string);

    public function toXml();

    public function getBase();

    public function getName($element);

    public function toArray($xmlObj);

    public function fromArray($data, $xmlObj);

    public function newXmlObject($base = null);

    public function getXmlObject();

    public function setXmlObject($xmlObj);

    public function getImmediateChildren();

    public function getChildren($element, $ns);

    public function getImmediateAttributes();

    public function getAttributes($element, $ns);

    public function testForError($xmlObj);

    /**
     * @param $name
     * @param null $value
     * @param null $attributes
     * @return \R25\Contracts\Model\Node
     */
    public function getNode($name, $value = null, $attributes = null);

    public function setNodeClass($node_class);

}