<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 10:12 AM
 */

namespace R25\Converters;

use R25\Contracts\Converter\Converter;
use R25\Contracts\Model\Node;
use R25\Contracts\Xml\Xml;

class ArrayConverter implements Converter {

    protected $options;

    protected $data = [];

    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    public function create(Xml $xml)
    {
        $arr = $xml->toArray($xml->getXmlObject());
        $this->createFromArray($arr);
    }

    public function createFromArray($data)
    {
        $this->data = $data;
    }

    public function getRaw()
    {
        return $this->data;
    }

    public function toArray()
    {
        return $this->_rToArray($this->data);
    }

    protected function _rToArray($data)
    {
        $arr = [];
        foreach($data as $k => $v) {
            if ($v instanceof Node) {
                $arr[$k] = $v->toArray(true);
            }
            elseif (is_array($v)) {
                $arr[$k] = $this->_rToArray($v);
            }
            else {
                $arr[$k] = $v;
            }
        }
        return $arr;
    }
}