<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 9:54 AM
 */

namespace R25;


class Factory {

    protected $map = [];

    public function __construct($map)
    {
        $this->map = $map;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function create($name)
    {
        $args = func_get_args();
        array_shift($args);
        if ($this->has($name)) {
            $cls = $this->map[$name];
            if (count($args) === 0) {
                return new $cls;
            }
            $r = new \ReflectionClass($cls);
            return $r->newInstanceArgs($args);
        }
    }

    public function has($name)
    {
        return array_key_exists($name, $this->map);
    }

}