<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 12:35 PM
 */

namespace R25\Models\VO;


class Node implements \R25\Contracts\Model\Node {

    protected $name;
    protected $value;
    protected $attributes = [];
    protected $children = [];

    public function __construct($name, $value = null, $attributes = [])
    {
        $this->name = $name;
        if ($value) {
            $this->value = $value;
        }
        if ($attributes) {
            $this->attributes = $attributes;
        }
    }

    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->encode($this->value);
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        $atts = [];
        foreach($this->attributes as $k => $v) {
            $atts[$k] = $this->encode($v);
        }
        return $atts;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /** @return \R25\Contracts\Model\Node[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    public function hasChildren()
    {
        return count($this->children);
    }

    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->encode($this->attributes[$key]);
        }
    }

    public function addChild(\R25\Contracts\Model\Node $child)
    {
        $this->children[] = $child;
    }

    public function toArray($values_only = false)
    {
        $map_func = function($v) use(&$map_func, $values_only) {
            if ($v instanceof \R25\Contracts\Model\Node) {
                return $v->toArray($values_only);
            }
            else {
                return array_map($map_func, $v);
            }
        };
        if ($values_only) {
            return ['value' => $this->getValue(), 'children' => array_map($map_func, $this->getChildren())];
        }
        return [
            'value' => $this->getValue(),
            'attributes' => $this->getAttributes(),
            'children' => array_map($map_func, $this->getChildren()),
        ];
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->children) && $this->children[$key] instanceof \R25\Contracts\Model\Node) {
            return $this->children[$key]->getValue();
        }
    }

    public function __set($key, $value)
    {
        if (array_key_exists($key, $this->children) && $this->children[$key] instanceof \R25\Contracts\Model\Node) {
            return $this->children[$key]->setValue($value);
        }
    }

    public function encode($str)
    {
        return htmlspecialchars($str, ENT_XML1, 'UTF-8');
    }

}