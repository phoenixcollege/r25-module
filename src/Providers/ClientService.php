<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 9:00 AM
 */

namespace R25\Providers;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use R25\Request\Handler;
use Smorken\Service\Service;

class ClientService extends Service {

    public function start()
    {
        $this->name = 'client';
    }

    public function load()
    {
        $self = $this;
        $this->app->instance('cookiejar', function($c) {
            return new SessionCookieJar('r25_cookiejar', true);
        });
        $this->app[$this->getName()] = function($c) use ($self) {
            if ($self->app['config']->get('r25.client.test', false)) {
                return $self->testClient();
            }
            return $self->liveClient();
        };
    }

    protected function liveClient()
    {
        $copts = $this->app['config']->get('r25.client.options', []);
        if (isset($copts['cookies']) && $copts['cookies']) {
            $cj = $this->app['cookiejar'];
            $copts['cookies'] = $cj;
        }
        return new Client($copts);
    }

    protected function testClient()
    {
        return $this->getClient([
            'login_challenge.xml' => 200,
            'login_response.xml' => 200,
            'rm_reservations.121.xml' => 200,
            'rm_reservations.123.xml' => 200,
            'rm_reservations.126.xml' => 200,
            'rm_reservations.12276.xml' => 200,
        ]);
    }

    /**
     * @param $endpoint
     * @return Client
     */
    protected function getClient($endpoint)
    {
        $mock = new MockHandler($this->createResponses($endpoint));
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        return $client;
    }

    protected function createResponses($endpoints)
    {
        $responses = [];
        foreach($endpoints as $endpoint => $status_code) {
            $responses[] = new Response($status_code, [], $this->getXml($endpoint));
        }
        return $responses;
    }

    protected function getXml($endpoint)
    {
        $fp = $this->app['path.base'] . '/tests/functional/R25/data/' . $endpoint;
        return file_get_contents($fp);
    }
}