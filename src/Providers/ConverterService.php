<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 10:16 AM
 */

namespace R25\Providers;


use R25\Converters\ArrayConverter;
use Smorken\Service\Service;

class ConverterService extends Service {

    public function start()
    {
        $this->name = 'converter';
    }

    public function load()
    {
        $this->app[$this->getName()] = function($c) {
            return new ArrayConverter();
        };
    }
}