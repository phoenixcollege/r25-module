<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 1:30 PM
 */

namespace R25\Providers;


use R25\Request\Handler;
use R25\Xml\SimpleXml;
use Smorken\Service\Service;

class HandlerService extends Service {

    public function start()
    {
        $this->name = 'r25.handler';
    }

    public function load()
    {
        $app = $this->app;
        $this->app[$this->getName()] = function($c) use ($app) {
            $client = $app['client'];
            $xml = new SimpleXml();
            $conv = $app['converter'];
            $conf = $app['config']->get('r25.client', []);
            $h = new Handler($client, $xml, $conv, $conf);
            if ($app['cookiejar']) {
                $h->setCookieJar($app['cookiejar']);
            }
            return $h;
        };
    }
}