<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 2:58 PM
 */

namespace R25\Request;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Cookie\CookieJarInterface;
use Psr\Http\Message\ResponseInterface;
use R25\Contracts\Converter\Converter;
use R25\Contracts\Xml\Xml;

class Handler {

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Xml
     */
    protected $xml;

    /**
     * @var Converter
     */
    protected $converter;

    /**
     * @var CookieJarInterface
     */
    protected $cookiejar;

    protected $client_config = [];

    public function __construct(ClientInterface $client, Xml $xml, Converter $converter, array $client_config = [])
    {
        $this->client = $client;
        $this->xml = $xml;
        $this->converter = $converter;
        $this->client_config = $client_config;
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $options
     * @return Converter
     */
    public function requestAndConvert($method, $endpoint, array $options = [])
    {
        $r = $this->request($method, $this->createUrl($endpoint), $options);
        $x = $this->convertToXmlObject($r);
        return $this->convert($x);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $options
     * @return ResponseInterface
     */
    public function request($method, $uri, array $options = [])
    {
        if ($this->cookiejar) {
            $options['cookies'] = $this->getCookieJar();
        }
        $r = $this->getClient()->request($method, $uri, $options);
        return $r;
    }

    public function setCookieJar(CookieJarInterface $cookiejar)
    {
        $this->cookiejar = $cookiejar;
    }

    protected function getCookieJar()
    {
        return $this->cookiejar;
    }

    /**
     * Not used with Guzzle since it automatically throws an exception
     * @param $r
     * @throws RequestException
     */
    protected function handleInvalidResponse($r)
    {
        switch($r->getStatusCode()) {
            case 401:
                $err = 'Unauthorized';
                break;
            case 403:
                $err = 'You do not have permission to view the requested resource';
                break;
            default:
                $err = 'The server reported an error [' . $r->getStatusCode() . ']';
                break;
        }
        throw new RequestException($err);
    }

    /**
     * @param Xml $xml
     * @return Converter
     */
    public function convert(Xml $xml)
    {
        $this->getConverter()->create($xml);
        return $this->getConverter();
    }

    /**
     * @param ResponseInterface $response
     * @return Xml
     */
    public function convertToXmlObject(ResponseInterface $response)
    {
        $this->getXml()->createXmlObject((string)$response->getBody());
        return $this->getXml();
    }

    /**
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return Xml
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @return Converter
     */
    public function getConverter()
    {
        return $this->converter;
    }

    public function createUrl($endpoint)
    {
        return rtrim($this->getBaseUrl(), '/') . '/' . ltrim($endpoint, '/');
    }

    public function getBaseUrl()
    {
        if (array_key_exists('base_uri', $this->client_config)) {
            return $this->client_config['base_uri'];
        }
    }
}