<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 3:02 PM
 */

namespace R25\Services;

use R25\Request\Handler;

abstract class Base {

    protected $endpoint;

    /**
     * @var Handler
     */
    protected $handler;

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @return Handler
     */
    public function getHandler()
    {
        return $this->handler;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

}