<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 2:50 PM
 */

namespace R25\Services;


use GuzzleHttp\Cookie\SessionCookieJar;
use R25\Contracts\Converter\Converter;

class Login extends Base implements \R25\Contracts\Service\Login {

    protected $endpoint = 'login.xml';

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function login($username, $password)
    {
        $r = $this->getLoginResponse();
        if ($this->needsLogin($r)) {
            if ($this->setLoginElements($username, $password, $r)) {
                $xml = $this->getHandler()->getXml();
                $this->getHandler()->getXml()->fromArray(
                    $r->getRaw(),
                    $xml->newXmlObject('login_challenge')
                );
                $pr = $this->getHandler()->requestAndConvert('POST', $this->endpoint, [
                    'body' => $xml->toXml(),
                    'headers' => [
                        'Content-Type' => 'text/xml;charset=UTF-8',
                    ],
                ]);
                return !$this->needsLogin($pr);
            }
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * @return Converter
     */
    public function getLoginResponse()
    {
        return $this->getHandler()->requestAndConvert('GET', $this->endpoint);
    }

    /**
     * @param Converter $c
     * @return bool
     */
    public function needsLogin(Converter $c)
    {
        $r = $c->getRaw();
        $needs_login = !(isset($r['login'][0]) && $r['login'][0]->success === 'T');
        return $needs_login;
    }

    protected function setLoginElements($username, $password, Converter $c)
    {
        $r = $c->getRaw();
        if (isset($r['login'][0]) && $r['login'][0]->challenge) {
            $type = $r['login'][0]->challenge;
            if (substr($type, 0, 5) === 'Basic') {
                $response = $this->createBasicResponse($username, $password);
            }
            else {
                $response = $this->createMd5Response($username, $password, $type);
            }
            $r['login'][0]->username = $username;
            $r['login'][0]->response = $response;
            $r['login'][0]->challenge = null;
            return true;
        }
        return false;
    }

    protected function createBasicResponse($username, $password)
    {
        return 'Basic ' . base64_encode($username . ':' . $password);
    }

    protected function createMd5Response($username, $password, $challenge)
    {
        $pw = md5($password);
        $pw = $pw . ':' . $challenge;
        $pw = md5($pw);
        return $pw;
    }
}