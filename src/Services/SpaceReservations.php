<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/29/15
 * Time: 3:02 PM
 */

namespace R25\Services;


class SpaceReservations extends Base {

    protected $endpoint = 'rm_reservations.xml';

    public function getReservations($space_id, $start, $end)
    {
        $r = $this->getHandler()->requestAndConvert('GET', $this->endpoint, [
            'query' => [
                'space_id' => $space_id,
                'start_dt' => $start,
                'end_dt' => $end,
            ],
        ]);
        return $r;
    }
}