<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/28/15
 * Time: 2:04 PM
 */

namespace R25\Xml;


use R25\Contracts\Xml\Xml;

class SimpleXml implements Xml {

    protected $nodeClass = '\R25\Models\VO\Node';

    protected $namespaces = [];

    protected $default_namespaces = [
        'r25' => 'http://www.collegenet.com/r25',
        'xl' => 'http://www.w3.org/1999/xlink',
    ];

    protected $default_namespace = 'r25';

    /**
     * @var \SimpleXMLElement
     */
    protected $xmlObj;

    public function __construct($xml_string = null)
    {
        if ($xml_string !== null) {
            $this->createXmlObject($xml_string);
        }
    }

    public function createXmlObject($xml_string)
    {
        $o = simplexml_load_string($this->translateString($xml_string));
        if ($o === false) {
            throw new XmlException('Invalid XML', XmlException::BADXML);
        }
        $this->namespaces = $o->getNamespaces(true);
        $this->testForError($o);
        $this->xmlObj = $o;
    }

    /**
     * @param $name
     * @param null|string $value
     * @param array $attributes
     * @return \R25\Contracts\Model\Node
     */
    public function getNode($name, $value = null, $attributes = [])
    {
        return new $this->nodeClass($name, $value, $attributes);
    }

    public function setNodeClass($node_class)
    {
        $this->nodeClass = $node_class;
    }

    public function getBase()
    {
        return $this->getName($this->xmlObj);
    }

    public function getName($element)
    {
        return $element->getName();
    }

    protected function translateString($xml_string)
    {
        $trans_tbl = [
            chr(38) => '&amp;'
        ];
        return strtr($xml_string, $trans_tbl);
    }

    public function toXml()
    {
        return $this->xmlObj->saveXML();
    }

    public function toArray($xmlObj)
    {
        $arr = array();
        foreach($this->namespaces as $n => $urn) {
            foreach ($this->getChildren($xmlObj, $n) as $r) {
                if (count($this->getChildren($r, $n)) === 0) {
                    $arr[$r->getName()] = $this->getNode($r->getName(), (string)$r, $this->addAttributesToArray($r));
                } else {
                    $node = $this->getNode($r->getName(), null, $this->addAttributesToArray($r));
                    $node->setChildren($this->toArray($r));
                    $arr[$r->getName()][] = $node;
                }
            }
        }
        return $arr;
    }

    protected function addAttributesToArray($xmlObj)
    {
        $arr = [];
        $attrs = $this->getAllAttributes($xmlObj);
        foreach($attrs as $k => $v) {
            $arr[$k] = $v;
        }
        return $arr;
    }

    public function newXmlObject($base = null, $namespaces = [])
    {
        if ($base === null) {
            $base = 'root';
        }
        if (!$namespaces) {
            $namespaces = $this->default_namespaces;
        }
        if ($this->default_namespace && strpos($base, ':') === false) {
            $base = sprintf('%s:%s', $this->default_namespace, $base);
        }
        $base = sprintf('<?xml version="1.0"?><%s %s></%s>',
            $base,
            $this->namespacesToString($namespaces),
            $base
        );
        return new \SimpleXMLElement($base);
    }

    public function fromArray($data, $xmlObj)
    {
        foreach($data as $node) {
            if (is_array($node)) {
                $this->fromArray($node, $xmlObj);
            }
            else {
                $child = $xmlObj->addChild($node->getName(), (string)$node);
                foreach ($node->getAttributes() as $k => $v) {
                    $child->addAttribute($k, $v);
                }
                $this->fromArray($node->getChildren(), $child);
            }
        }
        $this->xmlObj = $xmlObj;
        return $this;
    }

    protected function namespacesToString($namespaces)
    {
        if ($namespaces) {
            $t = [];
            foreach($namespaces as $ns => $urn) {
                $t[] = sprintf('xmlns:%s="%s"', $ns, $urn);
            }
            return implode(' ', $t);
        }
        return null;
    }

    public function setXmlObject($xmlObj)
    {
        $this->xmlObj = $xmlObj;
    }

    public function getXmlObject()
    {
        return $this->xmlObj;
    }

    public function getImmediateChildren()
    {
        foreach($this->namespaces as $ns => $urn) {
            $c = $this->getChildren($this->xmlObj, $ns);
            if ($c) {
                return $c;
            }
        }
    }

    public function getChildren($element, $ns)
    {
        if ($ns) {
            return $element->children($this->namespaces[$ns]);
        }
        return $element->children();
    }

    public function getImmediateAttributes()
    {
        return $this->getAllAttributes($this->xmlObj);
    }

    protected function getAllAttributes($element)
    {
        $attrs = [];
        foreach($this->getAttributes($element) as $a => $v) {
            $attrs[$a] = (string)$v;
        }
        foreach($this->namespaces as $n => $url) {
            $t = $this->getAttributes($element, $n);
            foreach($t as $a => $v) {
                $attrs[$a] = (string)$v;
            }
        }
        return $attrs;
    }

    public function getAttributes($element = null, $ns = null)
    {
        $atts = [];
        if ($element === null) {
            $element = $this->xmlObj;
        }
        foreach($element->attributes($ns, true) as $a => $v) {
            $key = ($ns ? "$ns:$a" : "$a");
            $atts[$key] = (string)$v;
        }
        return $atts;
    }

    public function testForError($xmlObj)
    {
        foreach($this->namespaces as $ns => $urn) {
            $c = $this->getChildren($xmlObj, $ns);
            if ($c->getName() === 'error') {
                $error_message = (string)$c[0]->msg;
                $msg_id = $c[0]->msg_id;
                $error_code = defined('XmlException::' . $msg_id) ? constant('XmlException::' . $msg_id) : 0;
                throw new XmlException($error_message, $error_code);
            }
        }
    }
}