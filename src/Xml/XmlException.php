<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/28/15
 * Time: 2:06 PM
 */

namespace R25\Xml;


class XmlException extends \Exception {

    const UNKNOWN = 0;
    const BADREQUEST = 1;
    const UNAUTHORIZED = 2;
    const BADXML = 99;

}