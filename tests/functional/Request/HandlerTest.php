<?php

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use R25\Request\Handler;
use R25\Xml\SimpleXml;
use R25\Converters\ArrayConverter;
use GuzzleHttp\Client;

class HandlerTest extends PHPUnit_Framework_TestCase {


    public function testCreateUrl()
    {
        $s = $this->getSUT('simple.xml');
        $this->assertEquals('http://foo.com/simple.xml', $s->createUrl('simple.xml'));
    }

    public function testRequestReturnsResponseInterface()
    {
        $s = $this->getSUT('simple.xml');
        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $s->request('GET', $s->createUrl('simple.xml')));
    }

    public function testConvertToXmlObjectReturnsXmlContract()
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $this->assertInstanceOf('R25\Contracts\Xml\Xml', $s->convertToXmlObject($r));
    }

    public function testConvertReturnsArray()
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $x = $s->convertToXmlObject($r);
        $this->assertTrue(is_array($s->convert($x)->getRaw()));
    }

    public function testConvertToArray()
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $x = $s->convertToXmlObject($r);
        $a = $s->convert($x)->toArray();
        $this->assertArrayHasKey('simple', $a);
        $this->assertCount(3, $a['simple']);
    }

    public function testRequestAndConvert()
    {
        $s = $this->getSUT('simple.xml');
        $a = $s->requestAndConvert('GET', 'simple.xml')->toArray();
        $this->assertArrayHasKey('simple', $a);
        $this->assertCount(3, $a['simple']);
    }

    public function testBadRequest()
    {
        $s = $this->getSUT('bad_request.xml');
        $this->setExpectedException('R25\Xml\XmlException');
        $a = $s->requestAndConvert('GET', 'bad_request.xml');
    }

    /**
     * @param $endpoint
     * @param int $response_code
     * @return Handler
     */
    protected function getSUT($endpoint, $response_code = 200)
    {
        $client = $this->getClient($endpoint);
        return new Handler($client, new SimpleXml(), new ArrayConverter(), ['base_uri' => 'http://foo.com']);
    }

    /**
     * @param $endpoint
     * @param int $response_code
     * @return Client
     */
    protected function getClient($endpoint, $response_code = 200)
    {
        $r = new Response($response_code, [], $this->getXml($endpoint));
        $mock = new MockHandler([
            $r,
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        return $client;
    }

    protected function getXml($endpoint)
    {
        $fp = __DIR__ . '/../../data/' . $endpoint;
        return file_get_contents($fp);
    }

}