<?php
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use R25\Converters\ArrayConverter;
use R25\Request\Handler;
use R25\Xml\SimpleXml;
use R25\Services\Login;

class LoginTest extends PHPUnit_Framework_TestCase {


    public function testNeedsLoginIsTrue()
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200]));
        $this->assertTrue($sut->needsLogin($sut->getLoginResponse()));
    }

    public function testNeedsLoginIsFalse()
    {
        $sut = new Login($this->getHandler(['login_exists.xml' => 200]));
        $this->assertFalse($sut->needsLogin($sut->getLoginResponse()));
    }

    public function testLoginAlreadyLoggedIn()
    {
        $sut = new Login($this->getHandler(['login_exists.xml' => 200]));
        $this->assertTrue($sut->login('foo', 'bar'));
    }

    public function testLoginNotLoggedInSuccess()
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200, 'login_response.xml' => 200]));
        $this->assertTrue($sut->login('foo', 'bar'));
    }

    public function testLoginNotLoggedInFails()
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200, 'login_response_fail.xml' => 200]));
        $this->assertFalse($sut->login('foo', 'bar'));
    }

    /**
     * @param $endpoint
     * @param int $response_code
     * @return Handler
     */
    protected function getHandler($endpoint)
    {
        $client = $this->getClient($endpoint);
        return new Handler($client, new SimpleXml(), new ArrayConverter(), ['base_uri' => 'http://foo.com']);
    }

    /**
     * @param $endpoint
     * @param int $response_code
     * @return Client
     */
    protected function getClient($endpoint)
    {
        $mock = new MockHandler($this->createResponses($endpoint));
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        return $client;
    }

    protected function createResponses($endpoints)
    {
        $responses = [];
        foreach($endpoints as $endpoint => $status_code) {
            $responses[] = new Response($status_code, [], $this->getXml($endpoint));
        }
        return $responses;
    }

    protected function getXml($endpoint)
    {
        $fp = __DIR__ . '/../../data/' . $endpoint;
        return file_get_contents($fp);
    }
}