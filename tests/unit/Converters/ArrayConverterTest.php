<?php

use \R25\Converters\ArrayConverter as Base;

class ArrayConverterTest extends PHPUnit_Framework_TestCase {

    public function testCreateFromArraySimple()
    {
        $a = [
            'simple' => [
                0 => [
                    'foo' => 'bar',
                    'fiz' => 'bat',
                    '@foo_attr' => 'abc',
                    '@ns:fiz_attr' => 'def',
                    'fizzle' => [
                        'foo' => 'bat',
                    ],
                ],
                1 => [
                    'foo' => 'bar1',
                    'fiz' => 'bat1',
                    '@foo_attr' => 'abc1',
                    '@ns:fiz_attr' => 'def1',
                    'fizzle' => [
                        'foo' => 'bat1',
                    ],
                ],
            ],
        ];
        $m = new Base();
        $m->createFromArray($a);
        $data = $m->getRaw();
        $this->assertEquals($a, $data);
    }

    public function testToArrayWithModels()
    {
        $a = [
            'frazzle' => [
                0 => [
                    'foo' => 'bar',
                    'fiz' => 'bat',
                    '@foo_attr' => 'abc',
                    '@ns:fiz_attr' => 'def',
                    'frazzle' => [
                        'foo' => 'bat',
                    ],
                ],
                1 => [
                    'foo' => 'bar1',
                    'fiz' => 'bat1',
                    '@foo_attr' => 'abc1',
                    '@ns:fiz_attr' => 'def1',
                    'frazzle' => [
                        'foo' => 'bat1',
                    ],
                ],
            ],
        ];
        $m = new Base();
        $m->createFromArray($a);
        $data = $m->toArray();
        $this->assertEquals($a, $data);
    }

}
