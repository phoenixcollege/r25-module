<?php

use \R25\Converters\ArrayConverter as Base;
use R25\Xml\SimpleXml;

class SpaceReservationsTest extends PHPUnit_Framework_TestCase {


    public function testCreateFromArray()
    {
        $m = new Base();
        $m->createFromArray($this->getArray());
        $a = $m->getRaw();
//        var_dump($a);
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertInstanceOf('\R25\Contracts\Model\Node', $a['space_reservation'][0]);
    }

    public function testCreateFromXml()
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $m = new Base();
        $m->create($x);
        $a = $m->getRaw();
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertInstanceOf('\R25\Contracts\Model\Node', $a['space_reservation'][0]);
    }

    public function testToArray()
    {
        $m = new Base();
        $m->createFromArray($this->getArray());
        $a = $m->toArray();
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertEquals("", $a['space_reservation'][0]['value']);
        $this->assertCount(2, $a['space_reservation']);
    }

    public function testCreateMultipleFromXml()
    {
        $xml = $this->getXml('rm_reservations.multiple.xml');
        $x = new SimpleXml($xml);
        $m = new Base();
        $m->create($x);
        $a = $m->getRaw();
        $this->assertCount(30, $a['space_reservation']);
    }

    protected function getArray()
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        return $arr;
    }

    protected function getXml($filename)
    {
        $fp = __DIR__ . '/../../data/' . $filename;
        return file_get_contents($fp);
    }

}