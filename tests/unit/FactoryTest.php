<?php

use R25\Factory;

class FactoryTest extends PHPUnit_Framework_TestCase {

    public function testSimpleConstructor()
    {
        $f = $this->getFactory();
        $this->assertInstanceOf('Simple', $f->create('simple'));
    }

    public function testConstructorWithArgsNoneProvided()
    {
        $f = $this->getFactory();
        $this->setExpectedException('PHPUnit_Framework_Error');
        $o = $f->create('args');
    }

    public function testConstructorWithArgs()
    {
        $f = $this->getFactory();
        $o = $f->create('args', 'foo', 'bar');
        $this->assertInstanceOf('ArgConstructor', $o);
        $this->assertEquals('foo', $o->arg1);
    }

    protected function getFactory()
    {
        return new Factory([
            'simple' => 'Simple',
            'args' => 'ArgConstructor',
        ]);
    }
}

class Simple {}

class ArgConstructor {

    public $arg1;
    public $arg2;

    public function __construct($arg1, $arg2) {
        $this->arg1 = $arg1;
        $this->arg2 = $arg2;
    }
}