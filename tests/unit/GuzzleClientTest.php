<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\HandlerStack;
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/28/15
 * Time: 1:46 PM
 */
class GuzzleClientTest extends PHPUnit_Framework_TestCase {

    public function testGetBodyWithXmlFile()
    {
        $filedata = file_get_contents(__DIR__ . '/../data/login_response.xml');
        $r = new Response(200, [], $filedata);
        $mock = new MockHandler([
            $r,
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $response = $client->request('GET', '/');
        $this->assertEquals($filedata, (string)$response->getBody());
    }
}