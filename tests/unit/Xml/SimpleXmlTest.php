<?php

use R25\Xml\SimpleXml;

class SimpleXmlTest extends PHPUnit_Framework_TestCase {

    public function testCreateThrowExceptionWithErrorKey()
    {
        $xml = $this->getXml('bad_request.xml');
        $this->setExpectedException('\R25\Xml\XmlException');
        $x = new SimpleXml($xml);
    }

    public function testGetBase()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $this->assertEquals('login_challenge', $x->getBase());
    }

    public function testGetName()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $this->assertEquals('login_challenge', $x->getName($x->getXmlObject()));
    }

    public function testGetImmediateChildren()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $c = $x->getImmediateChildren();
        $this->assertEquals('login', $c->getName());
    }

    public function testGetImmediateAttributes()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $a = $x->getImmediateAttributes();
        $this->assertArrayHasKey('pubdate', $a);
    }

    public function testGetChildren()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $c = $x->getChildren($x->getXmlObject(), 'r25');
        $this->assertEquals('login', $c->getName());
    }

    public function testGetAttributes()
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $a = $x->getAttributes();
        $this->assertArrayHasKey('pubdate', $a);
    }

    public function testGetAttributesComplex()
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $a = $x->getAttributes($x->getImmediateChildren());
        $this->assertEquals(['id' => 'CBJDNTc5NDEz', 'crc' => '00000022', 'status' => 'est'], $a);
    }

    public function testToArraySimple()
    {
        $xml = $this->getXml('simple.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $this->assertArrayHasKey('simple', $arr);
        $this->assertCount(3, $arr['simple']);
        $this->assertEquals('SIMPLE 1', $arr['simple'][0]->simple_name);
    }

    public function testToArrayComplex()
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $this->assertArrayHasKey('space_reservation', $arr);
        $this->assertCount(2, $arr['space_reservation']);
        $this->assertArrayHasKey('xl:href', $arr['space_reservation'][0]->getAttributes());
    }

    public function testFromArraySimple()
    {
        $xml = $this->getXml('simple.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('r:simples', [
            'r' => 'http://www.collegenet.com/r25',
            'xl' => 'http://www.w3.org/1999/xlink',
        ]);
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('3">457</r:simple_type_id><r:simple_empty/></r:simple></r:simples>
', $xmlObj->saveXml());
    }

    public function testFromArrayComplex()
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('r25:events', [
            'r25' => 'http://www.collegenet.com/r25',
            'xl' => 'http://www.w3.org/1999/xlink',
        ]);
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('<r25:creation_dt>2013-09-26T09:54:10-07:00</r25:creation_dt></r25:event></r25:events>
', $xmlObj->saveXml());
    }

    public function testFromArrayComplexDefaultNamespaces()
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('events');
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('<r25:creation_dt>2013-09-26T09:54:10-07:00</r25:creation_dt></r25:event></r25:events>
', $xmlObj->saveXml());
    }

    protected function getXml($filename)
    {
        $fp = __DIR__ . '/../../data/' . $filename;
        return file_get_contents($fp);
    }
}